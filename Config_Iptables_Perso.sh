#!/bin/sh

echo "+ ------====== SCRIPT DEBUT !  ======------"

echo "+Vider les tables actuelles"
iptables -t filter -F

echo "+Vider les règles personnelles"
iptables -t filter -X

echo "+Interdire toutes connexions entrante et sortante"
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT DROP

echo "+Règle anti-DOS"
iptables -t filter -A INPUT -p all -m hashlimit \
--hashlimit-name FiltreAntiDOS \
--hashlimit-above 1000/second \
--hashlimit-mode srcip \
--hashlimit-burst 1000 \
--hashlimit-htable-expire 600000 \
-j DROP

echo "+Ne pas casser les connexions etablies"
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

echo "+Autoriser loopback"
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT

echo "+SSH In : 2424"
iptables -t filter -A INPUT -p tcp --dport 2424 -j ACCEPT

echo "+DNS Out : 53"
iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT
iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT

echo "+NTP Out : 123"
iptables -t filter -A OUTPUT -p udp --dport 123 -j ACCEPT
##iptables -t filter -A OUTPUT -p tcp --dport 123 -j ACCEPT

echo "+HTTP + HTTPS Out : 80 + 443"
iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT

echo "+HTTP + HTTPS In : 80 + 443"
iptables -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT

echo "+Mail SMTP : 25 // TLS + SMTP : 465 & 587"
iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 465 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 587 -j ACCEPT

echo "+  LOG_IN-OUT"
iptables -t filter -A INPUT  -p all -m limit --limit 10/hour -j LOG --log-prefix "Iptables :"
iptables -t filter -A OUTPUT -p all -m limit --limit 10/hour -j LOG --log-prefix "Iptables :"

echo "+ ------====== SCRIPT TERMINE! ======------"
echo "+ Pour afficher votre configuration Netfilter, lancez:"
echo " - 'iptables -L -n -v'           pour la table 'Filter'"

## bye bye le reloud
## sudo iptables -I INPUT -s IP -j DROP
